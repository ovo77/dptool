import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import trange
import os
import time

start = time.time()

im_row=1080
im_col=1920
im_chn=1

#READ RAW IMAGE
filepath= r'D:\Project_main\sf300v2\0223_bad pixel\0317_20pcs_outward\BMP\V0C01_off_Gain1_IntLines1100_UEV1100.bmp'
# filepath= r'D:\Project_main\sf300v2\0223_bad pixel\retake0224\output_bmp\imx390_AEC01_off_Gain1_IntLines1125_UEV1125.bmp'
# filepath= r'D:\Project_main\sf300v2\0223_bad pixel\retake0224\output_bmp\ov2778_A6C03_on_Gain1_IntLines1125_UEV1125_IrR0.08.bmp'


basename = os.path.basename(filepath)
filename=os.path.splitext(basename)[0]

img=cv2.imread(filepath,0)
# COUNT MEAN/STD/THERSHOLD
val_mean=np.mean(img)
val_std=np.std(img)
# val_hold=val_mean-5*val_std
# val_hold=128
print(val_mean,val_std)#,val_hold)

img_mode=[]
if val_mean>128:
    img_mode=1 #bright image
else:
    img_mode=2 #dark image
print(img_mode)

# #SHOW THE BAD PIXEL (dark:Green/bright:Magenta)
def image_scan(readimg,mode,mean,std):
    if mode==1: #bright image
        val_hold=mean-5*std
        img_dp=np.ones((im_row,im_col,3))*255
        for j in range(im_col):
            for i in range(im_row):
                if readimg[i,j] < val_hold:
                    # img_dp[i,j,1] = readimg[i,j]
                    img_dp[i,j,1] = 0
                else:
                    img_dp[i,j,1] = 255
    elif mode==2: #dark image
        val_hold=mean+5*std
        img_dp=np.zeros((im_row,im_col,3))
        for j in range(im_col):
            for i in range(im_row):
                if readimg[i,j] > val_hold:
                    # img_dp[i,j,1] = darkimg[i,j]
                    img_dp[i,j,1] = 255
                else:
                    img_dp[i,j,1] = 0
    else:
        print('image_scan error')
    return img_dp

img_dp=image_scan(img,img_mode,val_mean,val_std)
# plt.imshow(img_dp)
# plt.show()
# img_dp=abs(img_dp-255)
# print(img_dp[:,:,0])
# cv2.imshow('show',img_dp)
# cv2.waitKey()
# print(np.count_nonzero(img_dp==255))

# SCAN THE NON-CORRECTABLE HOT PIXELS
r=8
s=4
roi=np.ones((r,r))
img_dp_roi=img_dp
roi_csv=[['DP value','row','col']]
for m in trange(0,(im_col-r),s):
    for n in range(0,(im_row-r),s):
            roi_value=img_dp[n:n+r,m:m+r,1]*roi
            roi_info=[np.sum(roi_value),n,m]
            roi_csv.append(roi_info)
            if img_mode==1:
                if roi_info[0]<255*8:
                    img_dp_roi[n:n+r,m:m+r,0]=np.ones((8,8))*255
                    crop_dp=img_dp[n-s:n+s+r,m-s:m+s+r]
                    crop_dp_rgb=crop_dp[:,:,::-1]
                    cv2.imwrite('DPZOOMIN_{}_{}_{}.png'.format(filename,n,m),crop_dp_rgb)
                else:
                    img_dp[n:n+r,m:m+r,2]=255
            elif img_mode==2:
                if roi_info[0]>255*9:
                    img_dp_roi[n:n+r,m:m+r,0]=np.ones((8,8))*255
                    crop_dp=img_dp[n-s:n+s+r,m-s:m+s+r]
                    crop_dp_rgb=crop_dp[:,:,::-1]
                    cv2.imwrite('DPZOOMIN_{}_{}_{}.png'.format(filename,n,m),crop_dp_rgb)
                else:
                    img_dp[n:n+r,m:m+r,2]=0
            else:
                print('error: scan non correctable pixel')

# WRITE IN CSV FILE: DEFECT PIXEL OUTPUT
timestr = time.strftime("%Y%m%d%H%M%S")
np.savetxt('DPinfo_{}_{}.csv'.format(filename,timestr),roi_csv,delimiter=',',fmt='% s')

# SHOW THE DETECTION IMAGE
img_dp_roi_rgb=img_dp_roi[:,:,::-1]
cv2.imwrite('DP_{}_{}.png'.format(filename,timestr),img_dp_roi_rgb)
end = time.time()
# print('output the image:DP_{}_{}.png'.format(filename,timestr))
print('Time:',end - start)

# plt.imshow(img_dp_roi_rgb)
# plt.show()
cv2.imshow('show result image',img_dp_roi_rgb)
cv2.waitKey()