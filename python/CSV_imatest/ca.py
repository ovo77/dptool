from array import array
import string
from xmlrpc.client import boolean
import pandas as pd
import numpy as np
import os

path="D:/Project_main/sf300v2/0223_bad pixel/0321_dpc_yuvcapture/DPC_Validation/ov2778_sfr_BMP/ov2778_def_result/"
fileExt="MTF.csv"
# print(os.listdir(path))
ca_csv=[]
file_csv=[]
ff='File'
rr='Edge profile'
ccaa='CA Area (% to corner)'
index_csc=[rr,ccaa]
for file in os.listdir(path):
    if file.endswith(fileExt):
        # print(os.path.join(path, file))
        # print(file)

        #擷取單csv資料
        # path="D:/Project_main/sf300v2/0223_bad pixel/0321_dpc_yuvcapture/DPC_Validation/ov2778_sfr_BMP/ov2778_def_result/"
        # file='ov2778_def_YBL 68_17_MTF.csv'
        ca_file=pd.read_csv(path+file,delimiter=',',error_bad_lines=False)
        # print(pd.DataFrame.from_dict(ca_file)[:8])

        file_csv.append(file)
        # print(ca_csv)

        # print(ca_file[ca_file['Imatest']==ff])
        # print(ca_file[ca_file['Imatest']==rr])
        # print(ca_file[ca_file['Imatest']==ccaa])
        cainfo=np.logical_or(ca_file['Imatest']==rr,ca_file['Imatest']==ccaa)
        aa=ca_file[cainfo].values.tolist()

        # print([aa[0][1],aa[-1][1]])
        ca_csv.append([aa[0][1],aa[-1][1]])
        # ca_csv.append(ca_file[cainfo].values.tolist())
        
        # cainfo_csv.to_csv('cainfo.csv',index=False)
        # cainfo_list=cainfo_csv
        # print(cainfo_list)
        # cainfo_csv.to_csv('cainfo.csv',index=False)
csv=pd.DataFrame(index=file_csv,columns=index_csc,data=ca_csv)
# print(ca_csv)
csv.to_csv('cainfo.csv',encoding='gbk')
# ca_arr=np.asarray(ca_csv)
# # print(ca_csv)
# print(type(ca_arr),len(ca_arr))
# np.savetxt('cainfo.csv',ca_arr, delimiter=" ", fmt ='%s')