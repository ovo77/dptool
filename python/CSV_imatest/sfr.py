from importlib.resources import path
from textwrap import indent
import pandas as pd
import numpy as np

path='D:/Project_main/sf300v2/0223_bad pixel/0321_dpc_yuvcapture/DPC_Validation/ov2778_sfr_BMP/ov2778_adj0_result/'
file='SFR_cypx.csv'
df=pd.read_csv(path+file)
column_names = list(df.columns)
# print(column_names)
roich=column_names.index('ROI Ch')
mtf50 = column_names.index('Secondary')
mtf30 = column_names.index('Secondary.1')
# print(roich,mtf30,mtf50)
roich_val=column_names[roich]
mtf50_val=column_names[mtf50+1]
mft30_val=column_names[mtf30+1]
# print(df[roich_val][1:])
# print(df[mtf50_val][1:])
# print(df[mft30_val][1:])
csv_data=pd.read_csv(path+file, usecols=[roich_val,mtf50_val,mft30_val])
print(csv_data[1:])
