﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using System.Diagnostics;
using System.IO;
using System.Threading;


namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void OpenFile() //開啟檔案
        {
            OpenFileDialog filename = new OpenFileDialog();
            filename.Filter = "BMP(*.bmp);PNG(*.png)|*.bmp;*.png";
            filename.FilterIndex = 1;
            filename.RestoreDirectory = true;
            filename.FileName = "";
            if (filename.ShowDialog() == DialogResult.OK)
            {
                //檔案路徑存至textBox1
                textBox1.Text = filename.FileName;
                textBox2.Text = "Please click 'Scan DP' button! " + Environment.NewLine + "---------------------" + Environment.NewLine;
            }
        }

        private string[] SaveFile() //輸出影像存至原始位置並新增資料夾
        {
            string[] fileinfo = new string[2];
            string filegetname = Path.GetFileNameWithoutExtension(textBox1.Text);
            string getpath = Path.GetDirectoryName(textBox1.Text) + @"\output\";
            if (!Directory.Exists(getpath))
            {
                Directory.CreateDirectory(getpath);
            }
            fileinfo[0] = filegetname;
            fileinfo[1] = getpath;
            textBox2.AppendText("FileName is " + filegetname + Environment.NewLine + "---------------------" + Environment.NewLine);
            return fileinfo;
        }


        struct DPdata
        {
            public int DP_x;
            public int DP_y;
        }


        private Tuple<Mat,List<DPdata>,int> FindDP()
        {
            Mat SrcImage = new Mat(textBox1.Text, ImreadModes.Grayscale);
            Mat grayImage = new Mat(textBox1.Text, ImreadModes.Color);
            List<DPdata> DPcsv = new List<DPdata>();

            ////計算影像亮度mean & std
            Mat val_mean = new Mat();
            Mat val_std = new Mat();
            Cv2.MeanStdDev(SrcImage, val_mean, val_std);
            int imagemode = 0;
            imagemode = (val_mean.At<double>(0) > 128) ? 1 : 2;
            textBox2.AppendText("Star find out defect pixels:" + Environment.NewLine);

            ////分別計算影像(bright=1 / dark=2)
            if (imagemode == 1) //brightimage
            {
                double val_th = val_mean.At<double>(0) - 5 * val_std.At<double>(0);
                textBox2.AppendText("This is bright image" + Environment.NewLine);//, val_th:" + val_th);
                for (int y = 0; y < SrcImage.Height; y++)
                {
                    for (int x = 0; x < SrcImage.Width; x++)
                    {
                        var pixel = SrcImage.Get<Vec3b>(y, x);
                        if (pixel.Item0 < val_th)
                        {
                            var newPixel = new Vec3b
                            {
                                Item0 = 0, // B
                                Item1 = 255, // G
                                Item2 = 0 // R
                            };
                            grayImage.Set(y, x, newPixel);
                            DPcsv.Add(new DPdata { DP_x = x, DP_y = y});
                        }
                        else
                        {
                            var newPixel = new Vec3b
                            {
                                Item0 = 255, // B
                                Item1 = 255, // G
                                Item2 = 255 // R
                            };
                            grayImage.Set(y, x, newPixel);
                        }
                    }
                }
            }
            else if (imagemode == 2) //darkimage
            {
                double val_th = val_mean.At<double>(0) + 5 * val_std.At<double>(0);
                textBox2.AppendText("This is dark image" + Environment.NewLine);//, val_th:" + val_th);
                for (int y = 0; y < SrcImage.Height; y++)
                {
                    for (int x = 0; x < SrcImage.Width; x++)
                    {
                        var pixel = SrcImage.Get<Vec3b>(y, x);
                        if (pixel.Item0 > val_th)
                        {
                            var newPixel = new Vec3b
                            {
                                Item0 = 0, // B
                                Item1 = 255, // G
                                Item2 = 0 // R
                            };
                            grayImage.Set(y, x, newPixel);
                            DPcsv.Add(new DPdata { DP_x = x, DP_y = y});
                        }
                        else
                        {
                            var newPixel = new Vec3b
                            {
                                Item0 = 0, // B
                                Item1 = 0, // G
                                Item2 = 0 // R
                            };
                            grayImage.Set(y, x, newPixel);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Reload your image", "Image mode is error",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }

            var DPlist = Tuple.Create<Mat, List<DPdata>,int>(grayImage,DPcsv,imagemode);
            textBox2.AppendText("---------------------" + Environment.NewLine);
            SrcImage.Dispose();
            return DPlist;
        }

        private void writeCSV(string path, string imgname,List<DPdata> data)
        {
            using (var file= new StreamWriter($@"{path}CSV_{imgname}_dpinfo.csv"))
            {
                file.WriteLine($"DP_x,DP_y,result");
                foreach (var item in data)
                {
                    file.WriteLine($"{item.DP_x},{item.DP_y}");
                }
            }
        }

        private void showmatvalue(Mat one) //顯示mat value
        {
            int h = one.Rows;
            int w = one.Cols;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    //byte value = one.At<byte>(i, j);
                    int value = one.At<Vec3b>(i, j)[1];
                    Console.Write(value + " ");
                }
                Console.WriteLine();
            }
        }
        
        private void calculateDP(Mat grayImage,int imagemode)
        {
            textBox2.AppendText("Chekck for specification " + Environment.NewLine);
            int r = 8; int s = 4;
            Mat one = new Mat(r, r, MatType.CV_8UC3, Scalar.All(1));
            //showmatvalue(one);
            Mat roi2n = new Mat(r, r, MatType.CV_8UC1, Scalar.All(255));
            for (int j =0; j < grayImage.Height - s; j += s)
            {
                for (int i = 0; i < grayImage.Width - s; i += s)
                {
                    Rect roi = new Rect(i, j, r, r);
                    Mat roiimage = new Mat(grayImage, roi);
                    //showmatvalue(roiimage);
                    Mat dproi = roiimage.Mul(one);
                    Scalar val_sum = Cv2.Sum(dproi);

                    Mat[] dpg = Cv2.Split(dproi);
                    Mat dp_rgb = new Mat();
                    if (imagemode == 2)
                    {
                        string roisum = val_sum.Val1.ToString(); //Val1為G-channel
                        if (int.Parse(roisum) > 9*255)//in dark image
                        {
                            Cv2.Merge(new Mat[] { dpg[0], dpg[1], roi2n }, dp_rgb);
                            Rect rect = new Rect(i, j, dp_rgb.Rows, dp_rgb.Cols);
                            Mat pos = new Mat(grayImage, rect);
                            dp_rgb.CopyTo(pos);
                            Cv2.ImShow("Red ROI is out of spec", grayImage);
                            Cv2.WaitKey();
                        }
                        else
                        {
                            dp_rgb = dproi;
                        }
                    }
                    else if (imagemode == 1)
                    {
                        string roisum = val_sum.Val2.ToString(); //Val2為R-channel
                        if (int.Parse(roisum) < 11*255)//in dark image
                        {
                            Cv2.Merge(new Mat[] { dpg[0], dpg[1], roi2n }, dp_rgb);
                            Rect rect = new Rect(i, j, dp_rgb.Rows, dp_rgb.Cols);
                            Mat pos = new Mat(grayImage, rect);
                            dp_rgb.CopyTo(pos);
                            Cv2.ImShow("Red ROI is out of spec", grayImage);
                            Cv2.WaitKey();
                        }
                        else
                        {
                            dp_rgb = dproi;
                        }
                    }
                }
            }
            textBox2.AppendText("---------------------" + Environment.NewLine);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void loadimg_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Mat loadimage = new Mat(textBox1.Text,ImreadModes.Color);
            pictureBox1.Image=OpenCvSharp.Extensions.BitmapConverter.ToBitmap(loadimage);
            loadimage.Dispose();
        }

        private void scan_Click(object sender, EventArgs e)
        {
            ////輸出影像存至原始位置並新增資料夾
            string[] filepathname = SaveFile();

            ////計時器起點
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();
            sw.Start();

            ////計算影像的DP位置與輸出DP影像
            var DPinfo = FindDP();
            Mat grayImage = DPinfo.Item1;
            List<DPdata> DPcsv = DPinfo.Item2;
            int imagemode = DPinfo.Item3;

            ////掃描超出規格的壞點
            calculateDP(grayImage, imagemode);
            
            /////輸出影像顯示於pictureBox2中
            pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(grayImage);
            textBox2.AppendText("save outputdata: " + Environment.NewLine + "@" + filepathname[1] + Environment.NewLine);

            ////存DP資料(.csv)
            writeCSV(filepathname[1], filepathname[0],DPcsv);
            textBox2.AppendText(">>CSV done" + Environment.NewLine);

            ////存DP影像(.png)
            Cv2.ImWrite($@"{filepathname[1]}output_{filepathname[0]}_dp.png", grayImage);
            textBox2.AppendText(">>PNG done" + Environment.NewLine);

            ////計時器終點
            sw.Stop();
            string timeresult1 = sw.Elapsed.TotalSeconds.ToString();
            textBox2.AppendText(Environment.NewLine + "Spend time: " + timeresult1 + " sec");

            grayImage.Dispose();
            DPcsv = null;
        }
    }
}
