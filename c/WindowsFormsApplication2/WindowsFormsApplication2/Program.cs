﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
//using OpenCvSharp;

namespace WindowsFormsApplication2
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            //Mat src = new Mat("tt.jpeg",ImreadModes.Grayscale);
            //Mat dst = new Mat();
            //Cv2.Canny(src, dst, 50, 200);
            //using (new Window("src img", src)) ;
            //using (new Window("dst img", dst)) ;
            //{
            //    Cv2.WaitKey();
            //}
        }
    }
}
